
public class Television {
    private String MANUFACTURER;
    private int SCREEN_SIZE;
    private Boolean powerOn;
    private int channel;
    private int volumn;
   
    private int MAX_VOLUMN;
    private int MIN_VOLUMN;
   
    public Television(String brand, int size){
        this.MANUFACTURER = brand;
        this.SCREEN_SIZE = size;
        this.powerOn = false;
        this.MIN_VOLUMN = 1;
        this.MAX_VOLUMN = 100;
    }
   
    public void setChannel(int station){
        this.channel = station;
    }
   
    public int getChannel(){
        return this.channel;
    }
   
    public int getVolumn(){
        return this.volumn;
    }
   
    public String getManufacturer(){
        return this.MANUFACTURER;
    }
   
    public int getScreenSize(){
        return this.SCREEN_SIZE;
    }
   
    public void powerOn(){
        this.powerOn = !this.powerOn;
        System.out.println("Tv is "+(this.powerOn? "on":"off"));
    }
   
    public void increaseVolume(){
        if(this.volumn<this.MAX_VOLUMN)
            this.volumn++;
    }
   
    public void decreaseVolume(){
        if(this.volumn>this.MIN_VOLUMN)
            this.volumn--;   
    }
}

