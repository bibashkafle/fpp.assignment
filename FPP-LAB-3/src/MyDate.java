import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Scanner;

public class MyDate {
		
		private LocalDate date;
		
		public MyDate(int month, int day, int year){
			date = LocalDate.parse(month+"-"+day+"-"+year, DateTimeFormatter.ofPattern("MM-dd-yyyy"));
		}
		
		public MyDate(String month, int day, int year){
			date = LocalDate.parse(month+" "+day+", "+year, DateTimeFormatter.ofPattern("MMM dd, yyyy"));
		}
		
		public MyDate(int dayOfYear, int year){
			 Calendar calendar = Calendar.getInstance();
			 calendar.set(Calendar.YEAR, year);
			 calendar.set(Calendar.DAY_OF_YEAR, dayOfYear);
			 
			 String month = String.valueOf(calendar.get(Calendar.MONTH));
			 month = month.length()==1 ? "0"+month : month;
			 
			 String day =  String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
			 day = day.length()==1 ? "0"+day : day;
			 
			 date = LocalDate.parse(month+"-"+day+"-"+calendar.get(Calendar.YEAR), DateTimeFormatter.ofPattern("MM-dd-yyyy"));
		
		}
		
		public void getDate(){
			
			System.out.println("Enter 1 for format: MM/DD/YYYY");
			System.out.println("Enter 2 for format: Month DD, YYYY");
			System.out.println("Enter 3 for format: DDD YYYY");
			System.out.println("Enter 4 to exit");

			System.out.println("Enter your choice:");
			Scanner in = new Scanner(System.in);
			int choice = in.nextInt();
			switch (choice) {
			case 1:
				System.out.println(date.format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));
				break;
			case 2:
				System.out.println(date.format(DateTimeFormatter.ofPattern("MMM dd, yyyy")));
				break;
			case 3:
				System.out.println(date.format(DateTimeFormatter.ofPattern("DDD yyyy")));
				break;
			case 4: 
				System.out.println("Program exit");
				break;
			default:
				break;
			}
		}
		
}
